from django.contrib import admin
from .models import Words, Comment, Url, Votes

# Register your models here.
#para cada uno de nuestras clases en models.py
admin.site.register(Words)
admin.site.register(Comment)
admin.site.register(Url)
admin.site.register(Votes)
