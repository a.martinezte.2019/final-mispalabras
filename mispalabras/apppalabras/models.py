from django.db import models
from django.contrib.auth.models import User

# Create your models here.


class Words(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    date = models.DateTimeField()
    type = models.TextField(default="Words")
    val = models.TextField()
    wiki_def = models.TextField()
    img = models.URLField()
    rae_def = models.TextField(default="")
    flickr = models.URLField(default="")
    meme = models.URLField(default="")


class Comment(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    date = models.DateTimeField()
    type = models.TextField(default="Comments")
    word = models.ForeignKey(Words, on_delete=models.CASCADE)
    val = models.TextField(blank=False)


class Url(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    date = models.DateTimeField()
    type = models.TextField(default="Urls")
    word = models.ForeignKey(Words, on_delete=models.CASCADE)
    val = models.URLField(blank=False)
    img = models.URLField(blank=False)
    description = models.TextField(default="")


class Votes(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    date = models.DateTimeField()
    word = models.ForeignKey(Words, on_delete=models.CASCADE)
    val = models.IntegerField(default=0)
    type = models.TextField(default="Votes")
