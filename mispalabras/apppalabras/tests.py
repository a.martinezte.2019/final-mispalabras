from django.test import TestCase

# Create your tests here.
#importante seguir https://djangotutorial.readthedocs.io/es/1.8/intro/tutorial05.html
#https://developer.mozilla.org/es/docs/Learn/Server-side/Django/Testing
#sino pones test_ y el metodo no funciona
from django.contrib.auth.models import User


class TestLogin(TestCase):

    def setUp(self):
        self.credentials = {
            'username': 'testuser',
            'password': '1234'}
        User.objects.create_user(**self.credentials)
        self.credentials = {
            'username': 'testuser',
            'password': '1234',
            'name': 'login'}

        #test para el registro de un nuevo usuario
    def test_register(self):
        self.client.post('/mispalabras/register',
                         {'username': 'usuario', 'password': '1234', 'name': 'register', 'email': '1234@yahoo.es'})
        response = self.client.post('/mispalabras/', {'username': 'usuario', 'password': '1234', 'name': 'login'},
                                    follow=True)
        self.assertTrue(response.context['user'].is_active)
        self.assertEqual(response.status_code, 200)

    def test_login_is_true(self):
        response = self.client.post('/mispalabras/', self.credentials, follow=True)
        self.assertTrue(response.context['user'].is_active)
        self.assertEqual(response.status_code, 200)

    def test_login_is_false(self):
        response = self.client.post('/mispalabras/', {'username': 'usuario_2', 'password': '5678', 'name': 'login'},
                                    follow=True)
        self.assertFalse(response.context['user'].is_active)
        self.assertEqual(response.status_code, 200)

    #test para saber si se ha cerrado sesión
    def test_logout(self):
        self.client.post('/mispalabras/', self.credentials, follow=True)
        res1 = self.client.get('/mispalabras/logout')
        response = self.client.get('/mispalabras/')
        self.assertFalse(response.context['user'].is_active)
        self.assertEqual(res1.status_code, 302)

    #test para saber si se elimina un usuario
    def test_delete(self):
        self.client.post('/mispalabras/', self.credentials, follow=True)
        self.client.get('/mispalabras/delete')
        response = self.client.post('/mispalabras/', self.credentials, follow=True)
        self.assertFalse(response.context['user'].is_active)
        self.assertEqual(response.status_code, 200)


class MipaginaTest(TestCase):

    def setUp(self):
        self.client.post('/mispalabras/register',
                         {'username': 'usuario', 'password': '1234', 'name': 'register', 'email': '1234@yahoo.es'})
        self.client.post('/mispalabras/', {'username': 'usuario_2', 'password': '5678', 'name': 'login'},
                         follow=True)
        self.client.post('/mispalabras/casa', {'name': 'save'})

    def test_mipagina(self):
        response = self.client.get('/mispalabras/users/usuario')
        self.assertEqual(response.status_code, 200)


class SearchWord(TestCase):

    def test_inicio(self):
        response = self.client.get('/mispalabras/')
        self.assertEqual(response.status_code, 200)

    def test_palabra_existe_get(self):
        self.client.post('/mispalabras/register',
                         {'username': 'usuario', 'password': '1234', 'name': 'register', 'email': '1234@yahoo.es'})
        self.client.post('/mispalabras/', {'username': 'usuario', 'password': '1234', 'name': 'login'},
                         follow=True)
        self.client.post('/mispalabras/casa', {'name': 'Guardar palabra'})
        response = self.client.get('/mispalabras/casa')
        content = response.content.decode('utf-8')
        self.assertEqual(response.status_code, 200)
        self.assertNotIn('Save', content)

    def test_palabra_noexiste_get(self):
        self.client.post('/mispalabras/register',
                         {'username': 'usuario', 'password': '1234', 'name': 'register', 'email': '1234@yahoo.es'})
        self.client.post('/mispalabras/', {'username': 'usuario', 'password': '1234', 'name': 'login'},
                         follow=True)
        response = self.client.get('/mispalabras/casa')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('Guardar palabra', content)

    def test_pagina_de_ayuda(self):
        response = self.client.get('/mispalabras/ayuda')
        self.assertEqual(response.status_code, 200)

    #xml_doc
    def test_xml(self):
        response = self.client.get('/mispalabras/?format=xml')
        self.assertEqual(response.status_code, 200)

    #json_doc
    def test_json(self):
        response = self.client.get('/mispalabras/?format=json')
        self.assertEqual(response.status_code, 200)


class TestWord(TestCase):

    def setUp(self):
        self.client.post('/mispalabras/register',
                         {'username': 'usuario', 'password': '1234', 'name': 'register', 'email': '1234@yahoo.es'})
        self.client.post('/mispalabras/', {'username': 'usuario', 'password': '1234', 'name': 'login'},
                         follow=True)
        self.client.post('/mispalabras/casa', {'name': 'save'})

    def test_comment(self):
        self.client.post('/mispalabras/casa', {'name': 'comment', 'valor': 'esto es una prueba'})
        response = self.client.get('/mispalabras/casa')
        content = response.content.decode('utf-8')
        self.assertIn('esto es una prueba', content)
        self.assertEqual(response.status_code, 200)

    def test_link(self):
        self.client.post('/mispalabras/casa', {'name': 'link', 'valor': 'http://www.marca.com'})
        response = self.client.get('/mispalabras/casa')
        content = response.content.decode('utf-8')
        self.assertIn('http://www.marca.com', content)
        self.assertEqual(response.status_code, 200)

    def test_informacion_automatica(self):
        response = self.client.get('/mispalabras/casa')
        content = response.content.decode('utf-8')
        self.assertIn('Obtener definición de la RAE', content)
        self.assertIn('Obtener imagen', content)
        self.assertEqual(response.status_code, 200)

    def test_is_flickr(self):
        self.client.post('/mispalabras/casa', {'name': 'flickr'})
        response = self.client.get('/mispalabras/casa')
        content = response.content.decode('utf-8')
        self.assertIn('Imagen de Flickr', content)
        self.assertEqual(response.status_code, 200)

    def test_is_rae(self):
        self.client.post('/mispalabras/casa', {'name': 'rae'})
        response = self.client.get('/mispalabras/casa')
        content = response.content.decode('utf-8')
        self.assertIn('1.', content)
        self.assertEqual(response.status_code, 200)

    def test_megusta(self):
        self.client.post('/mispalabras/casa', {'name': 'like'})
        response = self.client.get('/mispalabras/casa')
        content = response.content.decode('utf-8')
        self.assertIn('Votos: 1', content)
        self.assertEqual(response.status_code, 200)

    def test_nomegusta(self):
        self.client.post('/mispalabras/casa', {'name': 'dislike'})
        response = self.client.get('/mispalabras/casa')
        content = response.content.decode('utf-8')
        self.assertIn('Votos: -1', content)
        self.assertEqual(response.status_code, 200)

    def test_sinvoto(self):
        response = self.client.get('/mispalabras/casa')
        content = response.content.decode('utf-8')
        self.assertIn('Votos: 0', content)
        self.assertEqual(response.status_code, 200)







