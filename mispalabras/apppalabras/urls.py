from django.urls import path
from . import views

urlpatterns = [
    path('ayuda', views.ayuda),
    path('delete', views.delete_user),
    path('logout', views.logout_user),
    path('register', views.register),
    path('', views.pag_principal),
    path('users/<str:user>', views.mi_pagina),
    path('<str:word>', views.word)
]
