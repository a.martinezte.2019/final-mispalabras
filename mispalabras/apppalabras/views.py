import json
import operator
import random
import cloudscraper
import urllib
import datetime
from urllib.request import urlopen
from itertools import chain
from .models import Words, Comment, Url, Votes
from django.http import HttpResponse
from django.contrib.auth.models import User
from django.contrib.auth import logout, login, authenticate
from django.shortcuts import redirect
from django.core import serializers
from django.shortcuts import render
from xml.etree.ElementTree import parse
from bs4 import BeautifulSoup


# Create your views here.
# servimos la definicion de la wikipedia para una palabra
def get_wiki_def(request, word):
    not_definition = "Esta palabra no está en Wikipedia en español"
    try:

        url_1 = "https://es.wikipedia.org/w/api.php?action=query&format=xml&titles="
        url_2 = "&prop=extracts&exintro&explaintext"
        url = url_1 + word + url_2
        doc = parse(urlopen(url))
        definition = doc.find('query/pages/page/extract').text
        if definition is None:
            definition = not_definition
        return definition
    except:
        return not_definition


# servimos la foto de la wikipedia para una palabra
def get_wiki_img(request, word):
    url_1 = "https://es.wikipedia.org/w/api.php?action=query&titles="
    url_2 = "&prop=pageimages&format=json&pithumbsize=200"
    img_not_found = "https://imgp.whaleshares.io/pimgp/a/einstei1/p/image-not-found-shitpostfriday/0x0/https" \
                    "://img.whaleshares.io/wls-img/einstei1/d765e65f432e7e6f0d062616d19364ecdc5631da.png"
    url = url_1 + word + url_2
    query = str(urlopen(url).read().decode('utf-8'))
    object = json.loads(query)
    url_media = object['query']['pages']
    id = str(url_media).split("'")[1]
    try:
        url_img = url_media[id]['thumbnail']['source']
        # sino servimos una foto(imag not found)
    except KeyError:
        url_img = img_not_found
    return url_img


# servimos la definicion de la DRAE para una palabra
def get_drae(request, word):
    url_1 = "https://dle.rae.es/"
    not_definition = "Esta palabra no está en la DRAE"
    user_agent = "Mozilla/5.0 (X11) Gecko/20100101 Firefox/100.0"
    url = url_1 + word
    headers = {'User-Agent': user_agent}
    req = urllib.request.Request(url, headers=headers)
    with urllib.request.urlopen(req) as response:
        html = response.read().decode('utf-8')
    soup = BeautifulSoup(html, 'html.parser')
    definition = soup.find("meta", {"property": "og:description"})
    if not definition["content"].startswith("1."):
        return not_definition
    return definition["content"]


# servimos la foto de flickr para una palabra
def get_flickr(request, word):
    img_not_found = "https://imgp.whaleshares.io/pimgp/a/einstei1/p/image-not-found-shitpostfriday/0x0/https" \
                    "://img.whaleshares.io/wls-img/einstei1/d765e65f432e7e6f0d062616d19364ecdc5631da.png"
    try:

        url_1 = "https://www.flickr.com/services/feeds/photos_public.gne?tags="
        url = url_1 + word
        scraper = cloudscraper.create_scraper()
        soup = BeautifulSoup(scraper.get(url).text, 'html.parser')
        urlimg = soup.find('feed').findAll('entry')[0]
        for a in urlimg.findAll('link', {'rel': 'enclosure'}, href=True):
            link = a['href']
    except:
        link = img_not_found
    return link


# comprobar tarjeta embebida en el HTML de los enlaces
def get_embebida(request, url):
    try:
        user_agent = "Mozilla/5.0 (X11) Gecko/20100101 Firefox/100.0"
        headers = {'User-Agent': user_agent}
        req = urllib.request.Request(url, headers=headers)
        with urllib.request.urlopen(req) as response:
            html = response.read().decode('utf-8')
        soup = BeautifulSoup(html, 'html.parser')
        description = soup.find("meta", {"property": "og:description"})["content"]
        img = soup.find("meta", {"property": "og:image"})["content"]
        if description is None:
            description = soup.find("meta", {"property": "og:title"})["content"]
        if description is None:
            description = ""
        if img is None:
            img = ""
        return description, img
    except:
        return "", ""


# control de todos los posts, si no esta logeado le registramos, sino acceso a todo
def forms_control(request):
    text = ""
    option = ""
    data = ""
    #string_1 = "/mispalabras/"
    #req_valor = request.POST['val']
    req_name = request.POST['name']
    value = ""
    if req_name == "login":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        # ahora si esta autenticado podemos: like, dislike, Enlace, comentar, guardar pal, meme, flickr, RAE, quitar voto
        if user is not None:
            login(request, user)
    elif req_name == "like":
        data = "like"
    elif req_name == "dislike":
        data = "dislike"
    elif req_name == "link":
        data = "link"
    elif req_name == "comment":
        data = "comment"
    elif req_name == "save":
        data = "save"
    elif req_name == "meme":
        option = request.POST['memeoption']
        text = request.POST['text']
    elif req_name == "flickr":
        data = "flickr"
    elif req_name == "rae":
        data = "rae"
    elif req_name == "remove_vote":
        data = "remove_vote"
    elif req_name == "word":
        value = "/mispalabras/" + request.POST['val']
    return value, option, text, data


# contador de palabras en la base de datos
def count_words():
    return Words.objects.all().__len__()


# obtener una palabra random (usar modulo python)
def random_word():
    word_list = Words.objects.all().values_list('val')
    if word_list.__len__() != 0:
        return str(random.choice(word_list)).split("'")[1]
    else:
        return ""


# lista de palabras votadas (TOP 10 mas votadas)
def voted_list():
    word_list = {}
    words = Words.objects.all()
    for word in words:
        n_votes = 0
        votes = Votes.objects.filter(word=word)
        for vote in votes:
            n_votes += vote.val
        word_list[word.val] = n_votes
    # ordenamos la lista de palabras (sorted) y devolvemos la lista ordenada (de mas reciente a menos)
    sorted_list = sorted(word_list.items(), key=operator.itemgetter(1), reverse=True)[0:10]
    final_sorted_list = []
    for word in sorted_list:
        final_sorted_list.append(word[0])
    return final_sorted_list


# lista de las palabras de la pagina principal (con su numero de votos)
def words_list(request, page_number):
    word_list = []
    words = Words.objects.all().values()
    sorted_list = sorted(words, key=lambda x: x['date'], reverse=True)
    for word in sorted_list:
        w = Words.objects.get(val=word['val'])
        n_votes = 0
        votes = Votes.objects.filter(word=w).values()
        for vote in votes:
            n_votes += vote['val']
        # guardamos en un dict los datos importantes de cada palabra
        word_dict = {'votes': n_votes, 'word': w}
        word_list.append(word_dict)
    return word_list[5 *(page_number - 1):(5 + 5 *(page_number - 1))]


# registrar usuario
def register(request):
    format_type = request.GET.get('format', None)
    if format_type:
        if format_type == 'xml':
            data = serializers.serialize("xml", Words.objects.all())
            return HttpResponse(data, content_type="text/xml")
        elif format_type == 'json':
            data = serializers.serialize("json", Words.objects.all())
            return HttpResponse(data, content_type="text/json")
    print("no entra")
    if request.method == 'POST' and (request.POST['name'] == 'register'):
        print("entra")
        username = request.POST['username']
        try:
            user = User.objects.get(username=username)
        except User.DoesNotExist:
            email = request.POST['email']
            password = request.POST['password']
            newuser = User.objects.create_user(username, email, password)
            login(request, newuser)
            return redirect('/mispalabras')
    x_words = count_words()
    e_word = random_word()
    mipagina = False
    template = 'register.html'
    sorted_word_list = voted_list()
    page_data = {'vote_list': sorted_word_list, 'mipagina': mipagina, 'x_words': x_words, 'e_word': e_word}
    # devolver la plantilla y los datos de la pagina(return principal)
    return render(request, template, page_data)


# eliminar usuario
def delete_user(request):
    usuario = request.user
    logout(request)
    usuario.delete()
    return redirect('/mispalabras')


# cerrar sesion del usuario
def logout_user(request):
    logout(request)
    return redirect('/mispalabras')


# pagina principal
def pag_principal(request):
    if request.method == 'POST':
        value = forms_control(request)
        if value[0]:
            return redirect(value[0])
    format_type = request.GET.get('format', None)
    if format_type:
        if format_type == 'xml':
            data = serializers.serialize("xml", Words.objects.all())
            return HttpResponse(data, content_type="text/xml")
        elif format_type == 'json':
            data = serializers.serialize("json", Words.objects.all())
            return HttpResponse(data, content_type="text/json")
    # numero de paginas del paginario
    x_words = count_words()
    x_pages = x_words // 5
    if (x_words % 5) != 0:
        x_pages += 1
    if x_pages != 0:
        list_x_pages = list(range(1, x_pages + 1))
    else:
        x_pages = 1
        list_x_pages = list(range(1, 2))
    page = int(request.GET.get('page', 1))
    if x_pages == 1:
        next_p = 1
        prev_p = 1
    else:
        if page == 1:
            next_p = page + 1
            prev_p = page
        elif page == x_pages:
            next_p = page
            prev_p = page - 1
        else:
            next_p = page + 1
            prev_p = page - 1
    sorted_word_list = voted_list()
    pages_list = words_list(request, page)
    mipagina = False
    e_word = random_word()
    page_data = {'vote_list': sorted_word_list, 'mipagina': mipagina, 'x_words': x_words, 'e_word': e_word,
                 'x_pages': list_x_pages, 'next': str(next_p), 'previous': str(prev_p), 'current': str(page),
                 'lista': pages_list}
    template = 'inicio.html'
    return render(request, template, page_data)


# mi pagina (pagina que solo se ves si estas autentificado), con comentarios, enlaces, palabras de cada usuario propio
def mi_pagina(request, user):
    if request.method == 'POST':
        value = forms_control(request)
        if value[0]:
            return redirect(value[0])
    format_type = request.GET.get('format', None)
    if format_type:
        if format_type == 'xml':
            data = serializers.serialize("xml", Words.objects.all())
            return HttpResponse(data, content_type="text/xml")
        elif format_type == 'json':
            data = serializers.serialize("json", Words.objects.all())
            return HttpResponse(data, content_type="text/json")
    sorted_word_list = voted_list()
    mipagina = True
    e_word = random_word()
    x_words = count_words()
    urls = Url.objects.filter(user=request.user).values()
    words = Words.objects.filter(user=request.user).values()
    comments = Comment.objects.filter(user=request.user).values()
    user_list = list(chain(words, comments, urls))
    user_list = sorted(user_list, key=lambda x: x['date'], reverse=True)
    page_data = {'vote_list': sorted_word_list, 'mipagina': mipagina, 'x_words': x_words, 'e_word': e_word,
                 "user_list": user_list}
    template = 'mipagina.html'
    return render(request, template, page_data)


# ayuda (html de ayuda)
def ayuda(request):
    if request.method == 'POST':
        value = forms_control(request)
        if value[0]:
            return redirect(value[0])
    format_type = request.GET.get('format', None)
    if format_type:
        if format_type == 'xml':
            data = serializers.serialize("xml", Words.objects.all())
            return HttpResponse(data, content_type="text/xml")
        elif format_type == 'json':
            data = serializers.serialize("json", Words.objects.all())
            return HttpResponse(data, content_type="text/json")
    sorted_word_list = voted_list()
    mipagina = False
    e_word = random_word()
    x_words = count_words()
    page_data = {'vote_list': sorted_word_list, 'mipagina': mipagina, 'x_words': x_words, 'e_word': e_word}
    template = 'ayuda.html'
    return render(request, template, page_data)


# pagina de la palabra(manejamos los votos del user a cada palabra y la info de cada palabra)
def word(request,word):
    rae = ""
    flickr = ""
    domain = '/mispalabras/'
    try:
        w = Words.objects.get(val=word)
        is_word = True
        definition = w.wiki_def
        url_img = w.img
        rae = w.rae_def
        flickr = w.flickr
        if request.method == 'POST':
            value = forms_control(request)
            if value[0]:
                return redirect(value[0])
        format_type = request.GET.get('format', None)
        if format_type:
            if format_type == 'xml':
                data = serializers.serialize("xml", Words.objects.filter(val=word))
                return HttpResponse(data, content_type="text/xml")
            elif format_type == 'json':
                data = serializers.serialize("json", Words.objects.filter(val=word))
                return HttpResponse(data, content_type="text/json")
        if request.method == 'POST':
            value = forms_control(request)
            if value[0]:
                redirect(value[0])
            if value[1]:
                w.meme = "https://apimeme.com/meme?meme=" + value[1] + "&top=" + word + "&bottom=" + value[2]
            # tenemos que hacer like, dislike, link, comment, save(si la palabra no existe), flickr, drae, remove_vote, word, para value[2]
            if value[3] == "like":
                valu = 1
                v = Votes(val=valu, word=w, user=request.user, date=datetime.datetime.now())
                v.save()
            # dislike igual que like pero con voto "negativo"
            elif value[3] == "dislike":
                valu = -1
                v = Votes(val=valu, word=w, user=request.user, date=datetime.datetime.now())
                v.save()
            elif value[3] == "link":
                link = request.POST['valor']
                description, img = get_embebida(request, link)
                if description and img:
                    url = Url(val=link, word=w, user=request.user, date=datetime.datetime.now(), img=img)
                else:
                    url = Url(val=link, word=w, user=request.user, date=datetime.datetime.now(), img=img,
                              description=description)
                url.save()
            elif value[3] == "comment":
                comment = request.POST['valor']
                c = Comment(val=comment, word=w, user=request.user, date=datetime.datetime.now())
                c.save()
            elif value[3] == 'flickr':
                w.flickr = get_flickr(request, word)
            elif value[3] == 'rae':
                w.rae_def = get_drae(request, word)
            elif value[3] == 'remove_vote':
                v = Votes.objects.get(user=request.user, word=w)
                v.delete()
            w.save()
            return redirect(domain + word)
        # falta comprobar si tenemos meme y def de la palabra
        if w.rae_def != "":
            is_rae = True
        else:
            is_rae = False
        # lo mismo para flckr
        if w.flickr != "":
            is_flickr = True
        else:
            is_flickr = False
        meme = w.meme
        votes = Votes.objects.filter(word=w)
        is_voted = False
        if request.user.is_authenticated:
            try:
                Votes.objects.get(user=request.user, word=w)
                is_voted = True
            except Votes.DoesNotExist:
                pass
        x_votes = 0
        for vote in votes:
            x_votes += vote.val
        comment_list = Comment.objects.filter(word=w)
        url_list = Url.objects.filter(word=w)
    except Words.DoesNotExist:
        comment_list = []
        url_list = []
        url_img = get_wiki_img(request, word)
        definition = get_wiki_def(request, word)
        is_voted = False
        is_word = False
        is_rae = False
        is_flickr = False
        meme = ""
        x_votes = 0
        domain = '/mispalabras/'
        # si la palabra no existe en nuestra base de datos hay que guardarla
        if request.method == 'POST':
            value = forms_control(request)
            if value[0]:
                return redirect(value[0])
            if value[3] == 'save':
                w = Words(val=word, user=request.user, date=datetime.datetime.now(), wiki_def=definition,
                          img=url_img)
                w.save()
                return redirect(domain + word)

    sorted_word_list = voted_list()
    e_word = random_word()
    x_words = count_words()
    page_data = {'vote_list': sorted_word_list, 'e_word': e_word, 'x_words': x_words, 'word': word,
                 'flickr': flickr, 'rae_def': rae, 'definition': definition, 'is_word': is_word, 'is_rae': is_rae,
                 'is_flickr': is_flickr, 'is_voted': is_voted, 'comments': comment_list, 'urls': url_list,
                 'votes': x_votes, 'meme_img': meme, 'image': url_img}
    template = 'palabra.html'
    return render(request, template, page_data)
