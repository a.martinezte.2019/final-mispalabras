
Práctica final curso 2021-2022 (MisPalabras)

## Getting started
## Entrega de la práctica


## Datos

- [ ] Nombre: Álvaro Martínez Téllez
- [ ] Titulación: Ingeniería en sistemas de telecomunicación
- [ ] Despliegue (url): http://amartinezte.pythonanywhere.com/mispalabras/
- [ ] Video funcionalidad básica (url): https://youtu.be/mRUaVtWqveo
- [ ] Video parte opcional (url): https://youtu.be/u5BnYhSmWMQ

## Cuenta admin site

- [ ] user/user

## Cuentas usuarios

- [ ] alvaro/alvaro    
- [ ] jorge/jorge 
- [ ] juan/juan
- [ ] luna/luna
- [ ] marcos/marcos
- [ ] pepe/pepe

## Resumen parte obligatoria

- [ ] Vamos a poder encontrar información de cualquier palabra que sea de interés del usuario, además de poder comentar
        sobre las mismas.
        Se puede buscar cualquier palabra. Si ya está almacenada en nuestra base de datos se mostrará la pagina de esa
        misma palabra con toda su información correspondiente. Si no está almacenada, nos aparecerá la imagen y la
        definición de wikipedia de la misma.
        Para poder añadir una palabra a nuestra base de datos es necesario estar autenticado, es decir estar logeado con
        un usuario y una contraseña.
        En todas las páginas podremos ver una lista de las TOP 10 palabras más votadas, además de un formulario para poder
        buscar una palabra.
- [ ] En la página principal vamos a poder encontrar un listado de todas las palabras que hay almacenadas en la base de datos, de 5 en 5, en
        el orden inverso en el que se han ido añadiendo. Además, para cada una de ellas vamos a tener su descripción,
        su imagen, el número de votos y el usuario que la añadió.
- [ ] Para la página de cada una de las palabras, vamos a poder ver su descripción e imagen de Wikipedia y la información
        automática añadida, como:
            -La definición de la RAE
            -La primera imagen de Flickr
            -El número de votos
            -Los enlaces añadidos
            -Su tarjeta embebida si tienen
            -Un Meme si se ha generado
            -Los comentarios.
        En caso de estar autenticados, podemos votar con like o dislike, quitar nuestro voto para esa palabra,
        añadir enlaces y comentarios, y cambiar el meme.
- [ ] Respecto a "Mipagina", solo vamos a poder acceder a esta página si estamos autenticados, veremos una página de usuario, en la que aparecerán, por orden inverso de creación,
        todas las aportaciones del usuario.
- [ ] Vamos a poder obtener el contenido de la página en formato XML o JSON.

# Lista partes opcionales

- [ ] Inclusión del favicon (imagen y texto)
- [ ] Pagina en formato JSON y XML de una sola palabra (cuando clicamos en JSON DOC o XML DOC dentro de la pag de la palabra)
- [ ] Eliminar una cuenta de usuario, eliminando así todo lo que ha hecho ese usuario
- [ ] Botones de Me gusta, dislike y poder eliminar nuestro voto
